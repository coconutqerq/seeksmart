require 'test_helper'

class IngredientsControllerTest < ActionDispatch::IntegrationTest
  test "should get name:string" do
    get ingredients_name:string_url
    assert_response :success
  end

  test "should get description:string" do
    get ingredients_description:string_url
    assert_response :success
  end

  test "should get type:string" do
    get ingredients_type:string_url
    assert_response :success
  end

end
