class CreateDresses < ActiveRecord::Migration[5.0]
  def change
    create_table :dresses do |t|
      t.integer :price
      t.text :description

      t.timestamps
    end
  end
end
