class AnimalsController < ApplicationController
  #by including this line, we can remove the following line:
  #@animal = Animal.find(params[:id])
  before_action :set_animal, only: [:show, :edit, :update, :destroy]
  respond_to? :html, :js
  def new
    @animal = Animal.new
  end

  def index
    @animals = Animal.all
  end
  def edit
  end
  def create
    @animal = Animal.new(animal_params)
    if @animal.save
      flash[:notice] = "Animal picture has been created."
      redirect_to @animal
    else
      flash.now[:alert] = "Project has not been created."
      render "new"
    end
  end
  def post

  end
  def show
    @image_url = "/assets/animal" + @animal.id.to_s + ".png"
  end

  def update
    @animal.update(animal_params)

    flash[:notice] = "Animal has been updated"
    redirect_to @animal
  end

  def delete
    @animal.destroy
    flash[:notice] = "Animal has been deleted."
    redirect_to animals_path
  end

  private
  def set_animal
    @animal = Animal.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "The animal you are looking for cannot be found."
    redirect_to animals_path
  end
  def animal_params
    params.require(:animal).permit(:name,:description,:age)
  end
end
